﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LookAtObject : MonoBehaviour
{
    public GameObject objectToLookAt;

    /// According to https://docs.unity3d.com/ScriptReference/MonoBehaviour.LateUpdate.html,
    /// tracking/following cameras should be implemented using LateUpdate().
    void LateUpdate()
    {
        transform.LookAt(objectToLookAt.transform);
    }
}
