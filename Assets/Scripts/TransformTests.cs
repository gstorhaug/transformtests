﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TransformTests : MonoBehaviour
{
    public Vector3 forward;
    public Vector3 computedCameraPosition;
    public Vector3 computedForward;
    public Vector3 toPos;
    public Vector3 testPositionCameraCoords;
    public Vector3 testPositionComputed;
    public GameObject testPoint;
    public Vector3 testDirection;
    public Vector3 testDirectionNormalized;
    Matrix4x4 cameraToWorldMatrix;
    public float dotProduct;

    void Start()
    {
        
    }

    // Compute the dot product between the forward vector and a vector towards "testPoint".
    // Print the dot product if it is close to 1.0.
    void Update()
    {
        // This works
        forward = Camera.main.transform.forward;
        testDirection = testPoint.transform.position - transform.position;
        testDirectionNormalized = testDirection.normalized;
        dotProduct = Vector3.Dot(forward, testDirectionNormalized);
        // More complete tests (replicates more of the stuff currently in DebugSphere)
        cameraToWorldMatrix = Camera.main.transform.localToWorldMatrix;
        computedCameraPosition = cameraToWorldMatrix.MultiplyPoint3x4(Vector3.zero);
        computedForward = (cameraToWorldMatrix.MultiplyPoint3x4(Vector3.forward) - Camera.main.transform.position).normalized;
        testPositionCameraCoords = Camera.main.transform.worldToLocalMatrix.MultiplyPoint3x4(testPoint.transform.position);
        toPos = cameraToWorldMatrix.MultiplyPoint3x4(testPoint.transform.position - computedCameraPosition).normalized;
        if (dotProduct > 0.995)
        {
            Debug.Log("dotProduct:" + dotProduct);
        }
    }
}
