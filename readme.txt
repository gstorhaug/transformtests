Simple test to verify that some transforms and calculations works as expected - they do.

How to use: Select Main Camera and move it in the Scene windows.  Observe value changes in the Inspector (even when not running the scene).